package org.gcube.accounting.accounting.summary.access.impl;


import org.gcube.accounting.accounting.summary.access.model.ScopeDescriptor;
import org.gcube.common.authorization.utils.manager.SecretManager;
import org.gcube.common.authorization.utils.manager.SecretManagerProvider;

public class BasicContextTreeProvider implements ContextTreeProvider {

	@Override
	public ScopeDescriptor getTree(Object unuseful) throws Exception {

		SecretManager cm = SecretManagerProvider.instance.get();
		final String context = cm.getContext();
		
		return new ScopeDescriptor(context.substring(context.lastIndexOf("/")+1, context.length()),context);
		
	}

}
