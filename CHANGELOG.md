This project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

# Changelog for accounting-summary-access

## [v1.0.3] - 2020-09-03

Integrated auth-utils

### Fixes

- Integration with gcube distribution (https://support.d4science.org/issues/19612)